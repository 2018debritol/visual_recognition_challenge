import os
from tweet_collect.credentials import *
from TwitterAPI import TwitterAPI
from tweet_collect.check_media import give_path_photo_to_analyze
from image_cap.image_capturing import image_capture
from generate_text.generate_comment import generate_comment


def twitter_post(file_path, file_name, comment):
    """
    Posts the image and the comment at Twitter
    :param file_path: (str) path to the directory of the image
    :param file_name: (str) name of the image
    :param comment: (str) text to post with
    :return: nothing. Prints the request code
    """
    # Authenticate with our application credentials from credentials.py
    api = TwitterAPI(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_SECRET)

    # Search the image that will be post
    file = open(os.path.join(file_path,file_name), 'rb')
    data = file.read()

    # Make a post
    r = api.request('statuses/update_with_media', {'status': comment}, {'media[]': data})
    print("Request status:", r.status_code)


if __name__ == "__main__":
    print("[  0%] Initializing..")
    keywords = []
    path_file = ''
    image_name = ''

    while len(keywords) == 0:
        print("[ 20%] Downloading image..")
        path_file, image_name = give_path_photo_to_analyze()

        print("[ 40%] Analysing image..")
        keywords = image_capture(path_file, image_name)

    print("[ 60%] Generating comment..")
    comment = str(generate_comment(keywords))

    print("[ 80%] Posting tweet..")
    twitter_post(path_file, image_name, comment)

    print("[100%] Done!")
    print(comment)
