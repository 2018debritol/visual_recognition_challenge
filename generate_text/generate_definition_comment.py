from generate_text.data_opening import open_data
from PyDictionary import PyDictionary


def find_index(word, list):
    """
    Find the index of a string in a list of strings
    :param word: (str) word you want to find
    :param list: (list) of strings
    :return: (int) with the index of the object in the list or -1 if it doesn't find it
    """
    i = 0
    while i < len(list):
        if list[i] == word:
            return i
        i += 1
    return -1


def definition_comment(word):
    """
    Create a comment from a dictionary definition of it
    :param word: (str) word to create a comment
    :return: (str) the definition of the word or empty string if it doesn't find it
    """
    dictionary = PyDictionary()

    try:
        all_definitions = dictionary.meaning(word)
        return all_definitions['Noun'][0].capitalize() + '. '
    except:
        print("Definitions of", word, "not found")
        return ''


def comment_curiosities(word, filename):
    """
    Create a comment based in a word, from a definition of dictionary or curiosity in a database.
    :param word: (str) word to comment
    :param filename: (str) animals or furniture, to load database of curiosities
    :return: (str) comment generated
    """
    objects = open_data(filename)
    objects_curiosities = open_data(filename + '_curiosity')

    # the curiosities of an object is in the same index as it is in both lists
    index = find_index(word, objects)

    if index != -1 and objects_curiosities[index] != '':
        return objects_curiosities[index] + ' '
    else:
        return definition_comment(word)
