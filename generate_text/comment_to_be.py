from textblob import TextBlob,Word
import random as rd


def in_list_dict(element, dict_counted):
    """
    Auxiliary function to check if an item (dict) is in a list of dictionaries
    :param element: (dict) the element you want to check
    :param dict_counted: (list) of dictionaries
    :return: (Bool) if it's in list or not
    """
    for every_element in dict_counted:
        if Word.singularize(Word(element["name"])) == Word.singularize(Word(every_element["name"])):
            return True
    return False


def normalize_plural_dict(dict_objects):
    """
    Generates a list of dictionary of non repeated in plural or singular words,
    based in de dictionary that comes from the image treatment
    :param dict_objects:(dict)dictionary with a key "name" and "area" that comes
    from the image treatment
    :return: (list) of dicts of the keywords obtained from the image
    """
    dict_counted = []

    for i in range(len(dict_objects)):
        dict_objects[i]["quantity"] = 1
        for j in range(i+1, len(dict_objects)):
            if dict_objects[i]["name"] == dict_objects[j]["name"]:
                dict_objects[i]["name"] = Word(dict_objects[i]["name"]).pluralize()
                dict_objects[i]["quantity"] += 1
                dict_objects[i]["area"] += dict_objects[j]["area"]

    for element in dict_objects:
        if not in_list_dict(element, dict_counted):
            dict_counted.append(element)

    return list(dict_counted)


def comment_with_to_be(adjectives, noun):
    """
    Receive a list of adjectives and one noun and return a random commentary with verb to be
    linking the noun and an adjective
    :param adjectives: (list) list of strings
    :param noun: (string)
    :return: (string) commentary
    """
    singular_prenouns =["This","That"]
    plural_prenouns = ["These","Those"]
    commentary = []

    try:
        for adjective in adjectives:
            for singular_prenoun in singular_prenouns:
                if TextBlob(noun).tags == [(noun, 'NN')]:
                    commentary.append(singular_prenoun + " " + noun + " is " + adjective)

            for plural_prenoun in plural_prenouns:
                if TextBlob(noun).tags == [(noun, 'NNS')]:
                    commentary.append(plural_prenoun + " " + noun + " are " + adjective)

        return rd.choice(commentary)
    except:
        print(noun, 'is not a noun.')
        return ''
