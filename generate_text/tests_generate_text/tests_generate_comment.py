from pytest import *
from generate_text.generate_comment import *


def test_choose_article():
    assert choose_article("banana") == 'a'
    assert choose_article("ambition") == 'an'
    assert choose_article("banana") == 'a'


def test_open_data():
    with raises(FileNotFoundError):
        list_words = open_data("non-existing_file")


def test_separate_words():
    assert separate_words([{"name": "banana", "area": "1290"}]) == {'nouns': ["banana"], 'verbs': [], 'adj': [],
                                                                    'adv': []}
    assert separate_words([{"name": "called", "area": "1290"}]) == {'nouns': [], 'verbs': ["called"], 'adj': [],
                                                                    'adv': []}
    assert separate_words([{"name": "red", "area": "1290"}]) == {'nouns': [], 'verbs': [], 'adj': ["red"], 'adv': []}


def test_calculate_word_polarity():
    assert calculate_word_polarity("aaaa") == 0


def test_find_max_area_object():
    assert find_max_area_object([{"name": "a", "area": "1000"}, {"name": "b", "area": "1290"}]) == "b"
    assert find_max_area_object([{"name": "a", "area": "2000"}, {"name": "b", "area": "1290"}]) == "a"
    assert find_max_area_object([{"name": "a", "area": "0"}, {"name": "b", "area": "-1000"}]) == "a"


if __name__ == "__main__":
    test_choose_article()
    test_open_data()
    test_separate_words()
    test_calculate_word_polarity()
    test_find_max_area_object()
