from pytest import *
from generate_text.generate_definition_comment import *


def test_find_index():
    assert find_index("teste", ["teste"]) == 0
    assert find_index("teste", ["lorem", "teste"]) == 1
    assert find_index('', []) == -1
    assert find_index('teste', []) == -1
    assert find_index('teste', ['lorem', 'ipsum']) == -1
    assert find_index('', ['teste', 'lorem']) == -1
    assert find_index("teste", ["lorem", "ipsum", "teste"]) == 2


if __name__ == "__main__":
    test_find_index()
