import os


def open_data(filename):
    """
    Open the database of words in '../words_database/(filename).txt' and return a list of those words
    :param filename: (str) with the name of the word
    :return: list of words from the database
    """
    filename = os.path.join('../words_database/', filename + '.txt')
    list_words = []

    try:
        with open(filename, 'r') as file:
            list_raw = file.readlines()
            for word in list_raw:
                list_words.append(word.replace('\n', ''))

    except FileNotFoundError:
        raise FileNotFoundError("Look for your filename or words_database directory")

    return list_words
