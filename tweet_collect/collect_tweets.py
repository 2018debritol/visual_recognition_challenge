from tweet_collect import twitter_connection_setup #Import of twitter's API authentified


def collect(phrase,language,number):
    """
    This function collects the Status returned by
    the phrase in function of language that we're looking for
    :param phrase: phrase we're looking for
    :param language: language that we want to search
    :param number: number of tweets we want to
    :return: Number defined of Status of the tweets
    """

    API = twitter_connection_setup.twitter_setup() #API authentified
    tweets_status = API.search(phrase,language=language,rpp=number)

    return tweets_status #Return a list of tweets status


def collect_texts(phrase,language,number):
    """
    This function collects the Texts returned by
    the phrase in function of language that we're looking for
    :param phrase: phrase we're looking for
    :param language: language that we want to search
    :param number: number of tweets we want to
    :return: Number defined of Texts of the tweets
    """
    tweets_text = []

    #Collects the text in each Status
    for tweet in collect(phrase,language,number):
       tweets_text.append(tweet.text) #List with the texts of tweets

    return tweets_text #Return a list of tweets text


def collect_trends(trend_number):
    """
    This function collects the first 20 trends topics in USA
    :param trend_number: trend number that we're looking for
    :return: Number defined of Status of the tweets
    """

    API = twitter_connection_setup.twitter_setup() #API authentified
    trend_list = API.trends_place(23424977) #Localization of USA

    search_query = trend_list[0]['trends'][trend_number]['query'] #Return the query of the trend topic
    tweets_status = collect(search_query, 'english', 300) #Collect the tweets related to the trend topic in english

    return tweets_status #Return a list of tweets status

#Test of the function
if __name__ == "__main__":  # If we execute this file
    print(collect("CentraleSupélec", 'french', 10)) #Print our Status collected
    print(collect_texts("CentraleSupélec", 'french', 10)) #Print our Texts collected




