from pytest import *
from tweet_collect.collect_tweets import *
from tweet_collect.check_media import *
import tweepy
import os

#Modules tests collect_tweets
def test_collect():
    assert isinstance(collect("Good Morning",'english',10)[0], tweepy.models.Status)

def test_collect_trends():
    assert isinstance(collect_trends(10)[0], tweepy.models.Status)

def test_collect_texts():
    assert type(collect_texts('Good Morning','english',10)[0]) is str

#Modules tests check media
def test_check_tweet_url_and_photo():
    tweets = collect('Good Morning','english',100)
    result = check_tweet_url_and_photo(tweets)

    assert (type(result[0]) is str and type(result[1]) is str)

def test_search_random_tweet_with_photo():
    result = search_random_tweet_with_photo()
    assert (type(result[0]) is str and type(result[1]) is str)

def test_download_image():
    filename = download_image('http://pbs.twimg.com/media/Dsl8aCjX4AAsDz5.jpg')
    assert(type(filename) is str)

    os.remove(filename)



#For the function given_path_photo_to_analyze we don't need to test because it calls the others methods
#that were already tested



#Test of the functions
if __name__ == "__main__":  # If we execute this file
    #Modules tests collect_tweets
    test_collect()
    test_collect_trends()
    test_collect_texts()

    #Modules tests check media
    test_check_tweet_url_and_photo()
    test_search_random_tweet_with_photo()
