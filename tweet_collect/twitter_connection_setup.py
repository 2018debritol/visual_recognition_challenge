#Libraries and modules imports
import tweepy
from tweet_collect.credentials import * #We import ours credentials

def twitter_setup():
    """
    Utility function to setup the Twitter's API
    with an access keys provided in a file credentials.py
    :return: the authentified API
    """
    # Authentication and access using keys:
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)

    # Return API with authentication:
    API = tweepy.API(auth)
    return API

#Test of the function
if __name__ == "__main__":  # If we execute this file
    print(twitter_setup()) #Print our API
