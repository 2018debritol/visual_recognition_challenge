#Libraries and modules imports
from tweet_collect.collect_tweets import collect_trends
from random import randint,seed
import wget
import os

def check_tweet_url_and_photo(tweets_status):
    """
    This function tests if there's an url and after if that's a photo. First, we take a tweet and after we check if
    there's a media inside. If there's, we check if the media is a photo.
    :param tweets_status: Receive a list of tweets status
    :return: the url if exist, if not, return none
    """

    for tweet in tweets_status: #Check one tweet each time
        media_list_temp = tweet.entities.get('media',[])

        if len(media_list_temp) > 0: #Check if there's a media
            media_list = tweet.extended_entities.get('media',[]) #This check allows to know if there's a photo

            for media in media_list:
                if media['type'] == 'photo': #If the media is a photo
                    return media['media_url']

    return None #If there's no photo

def search_random_tweet_with_photo():
    """
    Search a random tweet that has a photo attached. Take a random trend topic and take a twitter from it
    :return: the url of the tweet founded
    """

    seed()
    numbers_searched = []

    for numbers_of_trends in range(0,20):
        """
        Take a random trend topic to search a tweet from it
        """
        trend_number = randint(0,20) #Generates a random number

        while trend_number in numbers_searched: #Check if the trend topic was already searched
            trend_number = randint(0,20)

        numbers_searched.append(trend_number) #Adds the trend topic that is used
        tweets = collect_trends(trend_number) #Collect the tweets

        url = check_tweet_url_and_photo(tweets) #Take the url founded

        if url != None: #If there's really a photo, give the url
            return url

    return None #If there's no photo, return None


def download_image(url_photo):
    """
    Take the photo url and downloads it
    :param url_photo: photo url to download
    :return: return de the file name
    """

    if url_photo != None:
        filename = wget.download(url_photo)

        return filename #Return filename

    print("Error - No Photo Founded") #If there's no photo, returns an error

    return None

def move_file(filename):
    """
    Move the image downloaded to the folder images
    :param filename: filename of the photo
    """

    if filename != None:
        current_directory = './' + filename
        new_directory = '../images/' + filename
        os.rename(current_directory,new_directory)


def rename_file(filename):
    """
    Change the image name for organisation.
    :param filename: filename of the photo
    :return: the path and the new name of the photo
    """
    if filename != None:
        path = '../images/'

        current_directory = path + filename

        new_filename = 'image_to_analyze.jpg'
        new_directory = path + new_filename

        if os.path.isfile(new_directory):
            os.remove(new_directory)

        os.rename(current_directory,new_directory)

        return path,new_filename


def give_path_photo_to_analyze():
    """
    Call the others modules to search and download an image from twitter
    :return: the image filepath
    """
    url = search_random_tweet_with_photo() #Take the url

    filename = download_image(url) #Save the image

   #Change the directory and image name
    move_file(filename)
    image_filepath = rename_file(filename)

    return image_filepath #Return the image filepath


############## Testing #################
if __name__ == "__main__": # If we execute this file
    print(give_path_photo_to_analyze()) #Give the path for the photo downloaded in twitter
