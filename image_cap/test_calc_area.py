from image_cap.image_capturing import calc_area
from pytest import *

def test_area():
    assert calc_area([2,7,4,9]) == 4
