from imageai.Detection import ObjectDetection
from PIL import Image
import os


def image_capture(image_path, image_name):
    """
    This function uses the library ImageAI to analyse an image and give the objects found in the image that have precision greater then 60%
    :param image_path: str that contains the path where the image is
    :param image_name: str that contains the image name
    :return: dict with keys ("name" and "area in the image") of the objects found in the image
    """
    try:
        detector = ObjectDetection()
        detector.setModelTypeAsRetinaNet()
        detector.setModelPath(os.path.join('../image_cap', "resnet50_coco_best_v2.0.1.h5"))
        detector.loadModel()

        # getting the objects in the image in a dictionary
        # detections = {"name": 'obj_name', "percentage_probability": float, "box_points": [x1, y1, x2, y2]}

        detections = detector.detectObjectsFromImage(input_image=os.path.join(image_path, image_name),
                                                     output_image_path=os.path.join('../images',
                                                                                    "object_" + image_name))

        # Storing the objects found in the image
        objects = []
        for eachObject in detections:
            area = calc_area(eachObject["box_points"], image_path + image_name)

            # getting the objects with higher than 60% of precision probability and
            # objects bigger than 0.5% of the image
            if eachObject["percentage_probability"] > 60 and area > 0.005:
                obj_dict = {"name": eachObject["name"], "area": area}
                objects.append(obj_dict)

        return objects
    except:
        print('The file cannot be opened, please check the given path / image name')


def calc_area(box_points, image_name):
    """
    Calculates the area of the object, for commenting over the higher area object
    :param box_points: they are the boundaries points of the object in the image
    :return: float: the area of the object
    """
    im = Image.open(image_name)
    width, height = im.size
    x = box_points[2] - box_points[0]
    y = box_points[3] - box_points[1]
    area = x * y / (width * height)

    return area


if __name__ == '__main__':

    image_path = '../images/'
    image_name = 'image_to_analyze.jpg'

    print(image_capture(image_path, image_name))
