# Visual Recognition Challenge

The program consists on image captioning from a tweet, and on automatically adding intelligent comments to the image.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

The program uses Python 3.6 and the following libraries:

* Tweepy
* TwitterAPI
* Tensorflow
* Numpy
* Matplotlib
* SciPy
* OpenCV (opencv-python)
* Pillow
* H5py
* Keras
* TextBlob
* ImageAI
* Operator
* Random
* os
* re
* PyDictionary
* PIL
* Wget

You will also need access to Twitter API. After [creating your account on Twitter](https://twitter.com/), it is necessary to go to the [application management portal](https://developer.twitter.com/en/apps) to create an app. Again, we enter here into a rather long process (a questionnaire with a lot of fields to complete) which goes through the submission of an application request to have a developer account and have access to the developer portal

### Installing

First, get all the libraries apart from ImageAI. For this, just install them with your usual methods

To install the ImageAI library, follow the instructions above:

* ImageAI

Open the prompt/terminal of your computer with admin permissions and type the following lines:
```
path/to/python/python.exe -m pip install --upgrade pip
path/to/python/python.exe -m pip install https://github.com/OlafenwaMoses/ImageAI/releases/download/2.0.2/imageai-2.0.2-py3-none-any.whl
```
If updating pip gives an error, just install an previous version of it with the command:
```
path/to/python/python.exe -m pip install pip==9.0.1
```
Then run the previous commands again

## Running the tests

There are files that gather the tests:
-test_comment_to_be.py
-test_generate_comment.py
-test_tweet_collect.py

### Break down into end to end tests

A test checks if given an entry for a function it returns the expected command
The tests returns True if the function did what was expected and False otherwise

```
Examples:
def test_collect_texts():
    assert type(collect_texts('Good Morning','english',10)[0]) is str

def test_collect_trends():
    assert isinstance(collect_trends(10)[0], tweepy.models.Status)
```

## Deployment

This program automatically creates comments for images related to the trending topics of a region.

## Built With

* [ImageAI](https://github.com/OlafenwaMoses/ImageAI) - The Image recognition program used

## Versioning

We use [GitLab CentraleSupelec](https://gitlab-student.centralesupelec.fr) for versioning. For the versions available, see the [tags on this repository](https://gitlab-student.centralesupelec.fr/2018debritol/visual_recognition_challenge/tags). 

## Group

* **Guilherme COTRIM FLEURY** - *Initial work*
* **João Victor EVANGELISTA MATOSO** - *Initial work*
* **Jorge KAIRALLA NETO** - *Initial work*
* **Lindemberg Samuel DE BRITO MATIAS** - *Initial work*
* **Raphaela SCARPEL BUENO** - *Initial work*
* **Vítor ALBUQUERQUE MARANHÃO RIBEIRO** - *Initial work*

See also the list of [contributors](https://gitlab-student.centralesupelec.fr/2018debritol/visual_recognition_challenge/contributors) who participated in this project.

## Acknowledgments

* To our mentors for helping us
* To our professors
